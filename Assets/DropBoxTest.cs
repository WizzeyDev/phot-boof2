﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DropBoxTest : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.D))
        {
            StartCoroutine(UploadFile());
        }
    }

    IEnumerator UploadFile()
    {
        print("Trying to upload?");
        //Uploading_File = true;
        //byte[] data = System.IO.File.ReadAllBytes("C:/Repository/photo_boof/photo_boof/Captures/As@_2020-03-12_12-16-51_540x960.mp4");
        //byte[] data =new byte[2];
        //var request = UploadManger.UploadFileDropbox(data, "dima.mp4");
        var request = UploadManger.DropBoxTest("CaptureVideos/dima.mp4");
        //var request = UploadManger.DropBoxTest("/1.txt");
        yield return new WaitForEndOfFrame();
        yield return request.Send().progress;
        while (!request.isDone)
        {
            //LoadingBar.fillAmount = progress + request.uploadProgress / 6.0f;
            yield return null;
        }
        switch (request.responseCode)
        {
            case 200:
                //UpdateStatus(TargetStatus.File_Uploaded);
                print("Uploaded to DropBox Yay");
                break;
            default:
                // No Internet Connection!
                Debug.LogError("DropBox Error " + request.downloadHandler.text);

                //UpdateStatus(TargetStatus.File_Upload_Failed);
                break;
        }
        print(request.responseCode + " text " + request.downloadHandler.text );//        print(request.responseCode + " text " + request.error + " Data: "+ request.downloadHandler.data.ToString());
    }
}
