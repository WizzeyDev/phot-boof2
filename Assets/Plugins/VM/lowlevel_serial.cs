﻿using System;
using System.Timers;
using System.IO.Ports;
using System.Threading;
using System.Diagnostics;

namespace app
{
    using com.bitmick.marshall.utils;
    using com.bitmick.marshall.protocol;

    public class lowlevel_serial : lowlevel_i
    {

        // ===========================================================
        // Constants
        // ===========================================================

        internal static readonly string TAG = typeof(lowlevel_serial).Name;

        internal static readonly int TIMER_TICK_MS = 100;

        // ===========================================================
        // Fields
        // ===========================================================

        private link_events_t m_link_events;

        private string m_serial_port_number;
        private int m_serial_port_baud_rate;
        private static SerialPort m_serial_port;

		private long m_last_tx_timestamp;

        // ===========================================================
        // Abstract methods
        // ===========================================================

        // ===========================================================
        // Constructors
        // ===========================================================

        // ===========================================================
        // Methods from SuperClass/Interfaces (and supporting methods)
        // ===========================================================

        override
        public void init(object i_f, object i_f_params)
        {
            m_serial_port_number = (string)i_f;
            m_serial_port_baud_rate = (int)i_f_params;
        }

        override
        public void register_link_events(link_events_t events)
        {
            m_link_events = events;
        }

        override
        public void start()
		{
            // open serial port
            m_serial_port = new SerialPort();
            m_serial_port.PortName = m_serial_port_number;
            m_serial_port.BaudRate = m_serial_port_baud_rate;
            m_serial_port.DataBits = 8;
            m_serial_port.StopBits = StopBits.One;
            m_serial_port.Parity = Parity.None;
			m_serial_port.WriteTimeout = -1;
			m_serial_port.ReadTimeout = -1;
            m_serial_port.WriteBufferSize = 16;
			m_serial_port.ReadBufferSize = 16;
			m_serial_port.Encoding = System.Text.Encoding.GetEncoding(28591);
			//m_serial_port.DiscardNull = true;

            try
            {
                m_serial_port.Open();

                // start parsing thread
                m_client_thread = new Thread(new ThreadStart(client_thread_proc));
                m_client_thread.Start();
            }
            catch (Exception ex)
            {
                Log.d(TAG, ex.Message);
            }
        }

		override
        public void stop()
        {
            try
            {
                // stop vmc link thread
                if (m_client_thread != null)
                {
                    m_client_thread_running = false;
                    m_client_thread = null;
                }
                // thanks Bob!
                if (m_serial_port.IsOpen)
                    m_serial_port.Close();
            }
            catch (Exception ex)
            {
                Log.d(TAG, ex.Message);
            }
        }

		override
		public void restart()
		{
			if (m_serial_port.IsOpen)
			{
				lock (m_serial_port)
				{
					m_serial_port.BaseStream.FlushAsync();
					m_serial_port.BaseStream.EndRead(null);
				}
			}
		}

		override
        public void dumpStats()
		{
			Log.d(TAG, string.Format("bytes available: {0} bytes", m_serial_port.BytesToRead));
		}

		override
        public bool transmit(byte[] data, int len)
        {
            lock (m_serial_port)
            {
                try
                {
					if (m_serial_port.IsOpen)
					{
						m_serial_port.Write(data, 0, len);
					}
                }
                catch (Exception ex)
                {
                    Log.d(TAG, ex.Message);
                }
            }

			m_last_tx_timestamp = Utils.currentTimeMillis();

            return true;
        }

		override
        public void onLinkTimerTick(long ms)
        {

			if ((ms - m_last_tx_timestamp) > 100)
				serial_reset();

        }

		// ===========================================================
        // Methods
        // ===========================================================
      
        private Thread m_client_thread;
		private bool m_client_thread_running;
        private int head;
        private int tail;

        private void serial_reset()
        {
            head = 0;
            tail = 0;
        }

        private int serial_read(byte[] buf, int offset)
        {
            int read_len = 0;

			lock (m_serial_port)
			{
				int to_read = Math.Min(m_serial_port.BytesToRead, (buf.Length - offset));
				if (to_read > 0)
				{
					lock (m_serial_port)
					{
						read_len = m_serial_port.Read(buf, offset, to_read);
					}
				}
			}

            return read_len;
        }

        private void client_thread_proc()
        {
			m_client_thread_running = true;

            byte[] rx_buf = new byte[4096 * 4];

            serial_reset();

			while (m_client_thread_running)
            {
				int read_len = serial_read(rx_buf, head);

                head += read_len;
    
                if (head != tail)
                {
					Boolean cont = true;

                    // try to handle as many packets that might be inside buffer
					do
					{
						int exp_len = ByteArrayUtils.byteArrToShort(rx_buf, tail) + 2;
						int rcv_len = head - tail;

						if (exp_len < 9 || exp_len > 512)
						{
							serial_reset();
						}
						else if (rcv_len >= exp_len)
						{
							// pass packet up
							if (m_link_events != null && !m_link_events.onReceive(rx_buf, tail, exp_len))
							{
								serial_reset();
							}
							else
							{
								tail += exp_len;

								// wrap around, only after packet processed succesfully
								if ((rx_buf.Length - tail) <= 412 && tail == head)
									serial_reset();
							}
       
						}

						cont = (head != tail) && (rcv_len >= exp_len);
					} while (cont);              
                }
                else
                {
					Thread.Sleep(1);
                }
            }
        }
    }
}