﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public static class DiskOnKey 
{
    public static string cliendInfosPath;

    public static void CheckIniFile() {
        //if (!File.Exists(Application.persistentDataPath + "/cliendInfosPath.txt")) {
        //    File.Create(Application.persistentDataPath + "/cliendInfosPath.txt");
        //}
        //cliendInfosPath = Application.persistentDataPath + "/cliendInfosPath.txt";
#if UNITY_EDITOR
#endif
        if (!File.Exists(Application.streamingAssetsPath + "/cliendInfosPath.txt")) {
            File.Create(Application.streamingAssetsPath + "/cliendInfosPath.txt");
        }
        cliendInfosPath = Application.streamingAssetsPath + "/cliendInfosPath.txt";
    }


    public static void SaveClient(string name,string email,string path) {
        StreamWriter writer = new StreamWriter(cliendInfosPath, true);

        string me = string.Format("[{0};{1};{2}]", name, email, path);
        writer.WriteLine(me);
        writer.Close();
    }

    public static string ReadString() {

        //Read the text from directly from the test.txt file
        StreamReader reader = new StreamReader(cliendInfosPath);
        string test = reader.ReadToEnd();
        test.Trim('\n');
            Debug.Log("Check "+ test);
        if (test.StartsWith("\n")) {
        }
        else {
            test.TrimStart('\n');
        }
         char[] be = { '[', ']' };
        var clientInfos = test.Split(be);
        foreach (var item in clientInfos) {
            if (!string.IsNullOrEmpty(item)) {
            if (!item.Contains("[" + "]") && item.Contains(";")) {

            //Debug.Log(item);
            }
            }
        }
        
        //var split = test.Trim('[', ']').Split(';'); 
        //test.
        //Debug.Log(reader.ReadToEnd());
        reader.Close();
        return test;
    }

    public static ClientInfo ConvertInfoTxtToObject(string value) {
        var split = value.Trim('[', ']').Split(';');

        ClientInfo clientInfo = new ClientInfo(split[0], split[1], split[3]);
        return clientInfo;
    }

    public static void CleanClientsData() {
        File.WriteAllText(Application.streamingAssetsPath + "/cliendInfosPath.txt",string.Empty);  
    }
}
