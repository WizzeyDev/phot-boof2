﻿using System.IO;
using System.Net;
using System.Net.Mail;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using OpenPop.Mime;
using Unity.Jobs;
using Unity.Collections;

public class EmailSender
{
    static int m_lastEmailCount;
    static string m_lastEmailSent;
    private const string from_email = "startsbox.info@gmail.com";
    private const string from_password = "rbggycwyimdvmocf";

    public static void SendEmail(string email, string title, string text)
    {      
        MailMessage mail = new MailMessage();

        mail.From = new MailAddress(from_email);
        mail.To.Add(email);
        mail.IsBodyHtml = true; // to use html tags in text, for example a newline tag <br />
        mail.Subject = title;
        mail.Body = text;

        SmtpClient smtpServer = new SmtpClient("smtp.gmail.com");
        smtpServer.Port = 587;
        smtpServer.Credentials = new System.Net.NetworkCredential(from_email, from_password) as ICredentialsByHost;
        smtpServer.EnableSsl = true;

        ServicePointManager.ServerCertificateValidationCallback =
            delegate (object s, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
            { return true; };


        try {
            smtpServer.SendAsync(mail,null);
            m_lastEmailSent = email;
        }
        catch (SmtpFailedRecipientsException ex) {
            UnityEngine.Debug.LogError(ex.GetBaseException());
            throw;
        }


    }

    public static bool isSendingMail;

    public static void SendEmail(string email, string title, string text, string filePath)
    {
        isSendingMail = true;
        MailMessage mail = new MailMessage();

        mail.From = new MailAddress(from_email);
        mail.To.Add(email);
        mail.IsBodyHtml = true;
        mail.Subject = title;
        mail.Body = text;
        if (filePath != null) {
           
            mail.Attachments.Add(new Attachment(filePath));

        }

        SmtpClient smtpServer = new SmtpClient("smtp.gmail.com");
        smtpServer.Port = 587;
        smtpServer.Credentials = new NetworkCredential(from_email, from_password) as ICredentialsByHost;
        smtpServer.EnableSsl = true;

        ServicePointManager.ServerCertificateValidationCallback =
            delegate (object s, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
            { return true; };


        smtpServer.SendAsync(mail,null);
            m_lastEmailSent = "";

        smtpServer.SendCompleted += (s, e) => {
            mail.Attachments.Clear();
            mail.Dispose();
            smtpServer.Dispose();
            isSendingMail = false;
            UnityEngine.Debug.Log("sended the mail ");
        };
        //mail.Attachments.Clear();
        //mail.Dispose();
        //smtpServer.Dispose();
        //isSendingMail = false;

    }

    static void OnCompleted(object sender, System.EventArgs a) {
  
    }


        public static bool FetchLastMessage() {
        var client = new OpenPop.Pop3.Pop3Client();
        client.Connect("pop.gmail.com", 995, true);
        client.Authenticate(from_email, from_password);
        
        var count = client.GetMessageCount();
        Message message = client.GetMessage(count);
        if (count > 0) {
            //MessageHeader headers = client.GetMessageHeaders(count);



            if (message.Headers.Subject.Contains("(Failure)")) {

                MessagePart plainText = message.FindFirstPlainTextVersion();

                if (plainText != null)

                    plainText.Save(new FileInfo("plainText.txt"));
                
                string me = File.ReadAllText("plainText.txt");
                if (me.Contains(m_lastEmailSent)) {
                    //TODO: ReMake Email
                    return true;
                }
            }


        }
        return false;
        //Console.WriteLine(message.Headers.Subject);
    }

    static string[] clientEmails, clientsNames, clientVideoPath;
    //static int emailsToSend= 10;

    public static void SendEmailJob(string emails, string names, string videoPath) {
        MailMessage mail = new MailMessage();

        mail.From = new MailAddress(from_email);
        mail.To.Add(emails);
        mail.IsBodyHtml = true;
        mail.Subject = "StarsBox Photo";
        mail.Body = names + " אתה כוכב אמיתי!!! עכשיו אפשר לצפות ולשתף את הסרטון שלך ";

        mail.Attachments.Add(new Attachment(videoPath));

        SmtpClient smtpServer = new SmtpClient("smtp.gmail.com");
        smtpServer.Port = 587;
        smtpServer.Credentials = new NetworkCredential("startsbox.info@gmail.com", from_password) as ICredentialsByHost;
        smtpServer.EnableSsl = true;

        ServicePointManager.ServerCertificateValidationCallback = delegate (object s, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors) { return true; };


        smtpServer.SendAsync(mail, null);

        smtpServer.SendCompleted += (s, e) => {
            mail.Attachments.Clear();
            mail.Dispose();
            smtpServer.Dispose();
            UnityEngine.Debug.Log("Complated Email Sending Async ");
            RecoveryEmailSender.EmailSendedCounter();
        };
    }
    //JobHandle EmailSenderWithJobs() {
    //    EmailJob job = new EmailJob();
    //    return job.Schedule();
    //}
}


public struct EmailJob : IJobParallelFor {



    private const string from_email = "startsbox.info@gmail.com";
    private const string from_password = "rbggycwyimdvmocf";


    public void Execute(int index) {

        MailMessage mail = new MailMessage();

        mail.From = new MailAddress(from_email);
        mail.To.Add(RecoveryEmailSender.clienEmails[index]);
        mail.IsBodyHtml = true;
        mail.Subject = "StarsBox Photo";
        mail.Body = RecoveryEmailSender.clienNames[index] + " אתה כוכב אמיתי!!! עכשיו אפשר לצפות ולשתף את הסרטון שלך ";

        mail.Attachments.Add(new Attachment(RecoveryEmailSender.clienVideoPath[index]));
      
        SmtpClient smtpServer = new SmtpClient("smtp.gmail.com");
        smtpServer.Port = 587;
        smtpServer.Credentials = new NetworkCredential("startsbox.info@gmail.com", from_password) as ICredentialsByHost;
        smtpServer.EnableSsl = true;

        ServicePointManager.ServerCertificateValidationCallback = delegate (object s, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors) { return true; };


        smtpServer.SendAsync(mail, null);

        smtpServer.SendCompleted += (s, e) => {
            mail.Attachments.Clear();
            mail.Dispose();
            smtpServer.Dispose();
            UnityEngine.Debug.Log("Complated Email Sending Async ");
            RecoveryEmailSender.EmailSendedCounter();
        };
    }
}

//public struct EmailJob :IJob {
 


//    private const string from_email = "startsbox.info@gmail.com";
//    private const string from_password = "rbggycwyimdvmocf";


//    public void Execute(int index) {
      
//        MailMessage mail = new MailMessage();

//        mail.From = new MailAddress(from_email);
//        mail.To.Add(EmailSender.Emails[index]);
//        mail.IsBodyHtml = true;
//        mail.Subject = "Job SYstem";
//        mail.Body = "Its Working?";
//        if (filePath != null) {
//            mail.Attachments.Add(new Attachment(filePath));
//        }

//        SmtpClient smtpServer = new SmtpClient("smtp.gmail.com");
//        smtpServer.Port = 587;
//        smtpServer.Credentials = new NetworkCredential("startsbox.info@gmail.com", from_password) as ICredentialsByHost;
//        smtpServer.EnableSsl = true;

//        ServicePointManager.ServerCertificateValidationCallback = delegate (object s, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors) { return true; };


//        smtpServer.SendAsync(mail, null);  

//        smtpServer.SendCompleted += (s, e) => {
//            mail.Attachments.Clear();
//            mail.Dispose();
//            smtpServer.Dispose();
//            UnityEngine.Debug.Log("sended the mail ");
//        };
//    }
//}
