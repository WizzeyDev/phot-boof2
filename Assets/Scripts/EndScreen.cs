﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EndScreen : MonoBehaviour
{
    public Button m_EndButton;

    string m_VideoPath;
    bool isNeedUpload,isVideoFinalized;
    static EndScreen instance;
    // Start is called before the first frame update
    void Awake()
    {
        instance = this;
        Application.runInBackground = true;
        //m_EndButton.interactable = false;
    }

    private void OnEnable() {
        if (StagesController.IsDropBoxing)
        {
            return;
        }
        StagesController.IsEmailBad = EmailSender.FetchLastMessage();
        if (StagesController.IsEmailBad) {
            print("Bad Email");
            StagesController.NEXTScreen();
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (StagesController.IsVideoFinalized) {
            StagesController.FailedVideoCapture();
            return; 
        }
        if (!StagesController.IsDropBoxing)
        {
            if (isNeedUpload && !StagesController.IsEmailBad && (new System.IO.FileInfo(m_VideoPath).Length / 1048576) > 0.5f)
            {
                isNeedUpload = false;
                if (Application.internetReachability == NetworkReachability.NotReachable)
                { //Save Client Data to file for later recovery
                    DiskOnKey.SaveClient(PlayerPrefs.GetString("Name"), PlayerPrefs.GetString("Account"), m_VideoPath);
                    StagesController.IsReEmailNeeded = true;
                    EndScreen.EmailedFhinshed();
                    return;
                }
                print("Sending mail with the video ");
                StartCoroutine("SendEmailDelayed"); //Send Email With Delay
            }
        }
        else //DropBox Sending
        {
            if (Application.internetReachability != NetworkReachability.NotReachable && isNeedUpload)
            {
                StartCoroutine("UploadToDropBoxDelayed"); //Send Email With Delay
                isNeedUpload = false;
            }
        }

        
    }

    IEnumerator UploadToDropBoxDelayed()
    {
        print("Starting DropBox Upload");
        var request = UploadManger.UploadCapturedToDropbox(m_VideoPath);
        //var request = UploadManger.DropBoxTest("/1.txt");
        yield return new WaitForEndOfFrame();
        yield return request.Send().progress;
        while (!request.isDone)
        {
      
            yield return null;
        }
        switch (request.responseCode)
        {
            case 200:
                if (StagesController.IsSmsing)
                {
                    StartCoroutine("ShareDPFile");
                    yield return null;
                }
                print("Uploaded to DropBox Yay");
                break;
            default:
              
                Debug.LogError("DropBox Error " + request.downloadHandler.text);

        
                break;
        }
        print(request.responseCode + " text " + request.downloadHandler.text);

        //UploadManger.UploadCapturedToDropbox(m_VideoPath);
     
        EmailedFhinshed();
    }

    IEnumerable ShareDPFile()
    {
        print("Starting ShareDPFile");
        var request = UploadManger.DPShareFile(m_VideoPath);
        yield return new WaitForEndOfFrame();
        yield return request.Send().progress;
        while (!request.isDone)
        {

            yield return null;
        }
        switch (request.responseCode)
        {
            case 200:              
                print("Got The Shared");
                break;
            default:

                Debug.LogError("DropBox Error " + request.downloadHandler.text);


                break;
        }
    }

        IEnumerator SendEmailDelayed() {
       yield return new WaitForSeconds(5f);
        EmailSender.SendEmail(PlayerPrefs.GetString("Account"), "StarsBox Photo", PlayerPrefs.GetString("Name") + " אתה כוכב אמיתי!!! עכשיו אפשר לצפות ולשתף את הסרטון שלך ", m_VideoPath);
       EmailedFhinshed();
    }



    public static void EmailedFhinshed() {
        instance.m_VideoPath = "";
        StagesController.IsEmailBad = false;
        StagesController.NEXTScreen();
        StagesController.IsVideoFinalized = false;
    }

    static string destinastionFile = "VideoCopyForMail.mp4";

    public static void TurnOnInteractive(string filepath) {
        //try {
        //    System.IO.File.Copy(filepath, destinastionFile, true);
        //}
        //catch (System.Exception) {
        //    Debug.LogError("Couldnt Copy File");
        //    throw;
        //}

        instance.m_VideoPath = filepath;

        instance.isNeedUpload = true;
    }


}
