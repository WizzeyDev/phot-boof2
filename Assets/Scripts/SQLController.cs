﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mono.Data.Sqlite;

public static  class SQLController 
{
    public static SqliteConnection mConnection;

    static string m_DBPath;
    public static void Init() {

    }

    public static SqliteConnection CurrentSQLConn() {
        if (mConnection == null) {
            if (!m_DBPath.StartsWith("URI=file:")) {
                m_DBPath = "URI=file:" + m_DBPath;
            }
            Debug.Log("SQLCOnnectsion Path: " + m_DBPath);
            mConnection = new SqliteConnection(m_DBPath);
            mConnection.Open();
            return mConnection;
        }
        else {
            return mConnection;
        }
    }

    //public static void CreateDBVmDB menuDB, SqliteConnection sqlConn, string dbName) {
    //    SqliteCommand insertSQL = new SqliteCommand("INSERT INTO " + dbName + " (Id,resturantID,price,loc,tab,tabName,descriptsion) VALUES (?,?,?,?,?,?,?)", sqlConn);
    //    insertSQL.Parameters.Add(new SqliteParameter("ID", menuDB.mID));
    //    insertSQL.Parameters.Add(new SqliteParameter("resturantID", menuDB.mResturantID));
    //    Debug.Log("resturantID " + menuDB.mResturantID + " p " + menuDB.mPrice);
    //    insertSQL.Parameters.Add(new SqliteParameter("price", menuDB.mPrice));
    //    insertSQL.Parameters.Add(new SqliteParameter("loc", menuDB.mLoc));
    //    insertSQL.Parameters.Add(new SqliteParameter("tab", menuDB.mTab));
    //    insertSQL.Parameters.Add(new SqliteParameter("tabName", menuDB.mName));
    //    insertSQL.Parameters.Add(new SqliteParameter("descriptsion", menuDB.mDescriptsion));
    //    try {
    //        insertSQL.ExecuteNonQueryAsync();
    //    }
    //    catch (System.Exception ex) {
    //        throw new System.Exception(ex.Message);
    //    }
    //}
}

[System.Serializable]
public class VmDB {
    public int mID, mResturantID, mPrice, mLoc, mTab;
    public string mName, mDescriptsion; 

    public VmDB(int id, int resturantID, int price, int loc, int tab, string name, string descrpitsion) {
        mID = id;
        mResturantID = resturantID;
        mPrice = price;
        mLoc = loc;
        mTab = tab;
        mName = name;
        mDescriptsion = descrpitsion;
    }
}
