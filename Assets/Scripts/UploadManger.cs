﻿using System;
using UnityEngine;
using UnityEngine.Networking;
using Unity.Mathematics;
using Unity.Jobs;
using System.IO;


public static class UploadManger
{
    private static string dropbox_access_token = "iYV2Udg15VAAAAAAAAAAOCz9cQdvubfLprVIh1TmrdPkFBYGU881u9n0g3fiGeBQ";

    public static UnityWebRequest UploadFileDropbox(byte[] data, string fileTargetPath) {
        string requestPath = "/upload";
        string serviceURI = @"https://content.dropboxapi.com/2/files" + requestPath;
        string contentType = "application/octet-stream";

        DropboxUploadParm dropData = new DropboxUploadParm();        // if your texture2d has RGb24 type, don't need to redraw new texture2d
        dropData.path = "/CaptureVideos/" + fileTargetPath;
        dropData.autorename = true;
        dropData.mode = "overwrite";
        UnityWebRequest request = new UnityWebRequest(serviceURI, "POST");
        request.SetRequestHeader("Authorization", string.Format("Bearer {0}", dropbox_access_token));
        request.SetRequestHeader("Content-Type", contentType);
        request.SetRequestHeader("Dropbox-API-Arg", JsonUtility.ToJson(dropData));
        Debug.Log("KOKACHO " + request.GetRequestHeader("Dropbox-API-Arg"));
        try {
            request.uploadHandler = (UploadHandler)new UploadHandlerRaw(data);
            Debug.Log("Uploaded?");
        }
        catch { }
        request.downloadHandler = (DownloadHandler)new DownloadHandlerBuffer();
        return request;
    }

    public static UnityWebRequest UploadCapturedToDropbox(string fileTargetPath)
    {
        byte[] data = System.IO.File.ReadAllBytes(fileTargetPath);

        string requestPath = "/upload";
        string serviceURI = @"https://content.dropboxapi.com/2/files" + requestPath;
        string contentType = "application/octet-stream";

        DropboxUploadParm dropData = new DropboxUploadParm();      
        dropData.path = "/CaptureVideos/" + Path.GetFileName(fileTargetPath);
        dropData.autorename = true;
        dropData.mode = "overwrite";
        UnityWebRequest request = new UnityWebRequest(serviceURI, "POST");
        request.SetRequestHeader("Authorization", string.Format("Bearer {0}", dropbox_access_token));
        request.SetRequestHeader("Content-Type", contentType);
        request.SetRequestHeader("Dropbox-API-Arg", JsonUtility.ToJson(dropData));
        Debug.Log("KOKACHO " + request.GetRequestHeader("Dropbox-API-Arg"));
        try
        {
            request.uploadHandler = (UploadHandler)new UploadHandlerRaw(data);
            Debug.Log("Uploaded?");
        }
        catch { }
        request.downloadHandler = (DownloadHandler)new DownloadHandlerBuffer();
        return request;
    }


    public static UnityWebRequest GetDropBoxShareLink(string fileTargetPath) {
        byte[] data = new byte[500];
        string requestPath = "/get_file_metadata";
        //string requestPath = "/share_folder";
        string serviceURI = @"https://api.dropboxapi.com/2/sharing" + requestPath;
        string contentType = "application/json";
  
        DropBoxGetFileData mPath = new DropBoxGetFileData();
        mPath.file = "/CaptureVideos/" + fileTargetPath;

        UnityWebRequest request = new UnityWebRequest(serviceURI, "POST");
        request.SetRequestHeader("Authorization", string.Format("Bearer {0}", dropbox_access_token));
        request.SetRequestHeader("Content-Type", contentType);
        string me = JsonUtility.ToJson(mPath);
        //me = me.Substring(0, me.Length - 3);
        //me = me + " " + JsonUtility.ToJson(dropData) + "}";
        Debug.Log(me);
        //request.SetRequestHeader("Dropbox-API-Arg", "settings" + JsonUtility.ToJson(dropData));
        //request.SetRequestHeader("Dropbox-API-Arg",me );
        //Debug.Log("KOKACHO " + request.GetRequestHeader("Dropbox-API-Arg"));
        try {
            data = System.Text.Encoding.ASCII.GetBytes(me);
            request.uploadHandler = (UploadHandler)new UploadHandlerRaw(data);

            Debug.Log("Uploaded?");
        }
        catch { }
        request.downloadHandler = (DownloadHandler)new DownloadHandlerBuffer();

        return request;
    }

    public static UnityWebRequest DropBoxTest(string fileTargetPath)
    {
        //byte[] data = new byte[500];
        DropTest2 dropData = new DropTest2();        
        dropData.path = "/" + fileTargetPath;

        string requestPath = "/create_shared_link_with_settings";
        string serviceURI = @"https://api.dropboxapi.com/2/sharing" + requestPath;
        string contentType = "application/json";

        UnityWebRequest request = new UnityWebRequest(serviceURI, "POST");
        request.SetRequestHeader("Authorization", string.Format("Bearer {0}", dropbox_access_token));
        request.SetRequestHeader("Content-Type", contentType);       
        byte[] data = System.Text.Encoding.UTF8.GetBytes(JsonUtility.ToJson(dropData));
    
        Debug.Log("KOKACHO " + data);
        try
        {
            request.uploadHandler = (UploadHandler)new UploadHandlerRaw(data);
            Debug.Log("DropBoxTest?");
        }
        catch { }
        request.downloadHandler = (DownloadHandler)new DownloadHandlerBuffer();
        return request;
    }

    public static UnityWebRequest DPShareFile(string fileTargetPath)
    {
        //byte[] data = new byte[500];
        DropTest2 dropData = new DropTest2();
        dropData.path = "/" + fileTargetPath;

        string requestPath = "/create_shared_link_with_settings";
        string serviceURI = @"https://api.dropboxapi.com/2/sharing" + requestPath;
        string contentType = "application/json";

        UnityWebRequest request = new UnityWebRequest(serviceURI, "POST");
        request.SetRequestHeader("Authorization", string.Format("Bearer {0}", dropbox_access_token));
        request.SetRequestHeader("Content-Type", contentType);
        byte[] data = System.Text.Encoding.UTF8.GetBytes(JsonUtility.ToJson(dropData));

        Debug.Log("KOKACHO " + data);
        try
        {
            request.uploadHandler = (UploadHandler)new UploadHandlerRaw(data);
            Debug.Log("DropBoxTest?");
        }
        catch { }
        request.downloadHandler = (DownloadHandler)new DownloadHandlerBuffer();
        return request;
    }

    //public static UnityWebRequest GetDropBoxShareLink(string fileTargetPath)
    //{
    //    byte[] data = new byte[500];
    //    string requestPath = "/create_shared_link_with_settings";
    //    string requestPath = "/share_folder";
    //    string serviceURI = @"https://api.dropboxapi.com/2/sharing" + requestPath;
    //    string contentType = "application/json";
    //    string contentType = "application/json";
    //    DropBoxSharingSettings dropData = new DropBoxSharingSettings();        // if your texture2d has RGb24 type, don't need to redraw new texture2d
    //    dropData.path = "/CaptureVideos/" + fileTargetPath;
    //    DropTest mPath = new DropTest();
    //    mPath.path = "/CaptureVideos/" + fileTargetPath;
    //    dropData.requested_visibility = "public";
    //    dropData.audience = "public";
    //    dropData.access = "editor";

    //    dropData.autorename = true;
    //    dropData.mode = "overwrite";
    //    UnityWebRequest request = new UnityWebRequest(serviceURI, "POST");
    //    request.SetRequestHeader("Authorization", string.Format("Bearer {0}", dropbox_access_token));
    //    request.SetRequestHeader("Content-Type", contentType);
    //    string me = JsonUtility.ToJson(mPath);
    //    me = me.Substring(0, me.Length - 3);
    //    me = me + " " + JsonUtility.ToJson(dropData) + "}";
    //    Debug.Log(me);
    //    request.SetRequestHeader("Dropbox-API-Arg", "settings" + JsonUtility.ToJson(dropData));
    //    request.SetRequestHeader("Dropbox-API-Arg", me);
    //    Debug.Log("KOKACHO " + request.GetRequestHeader("Dropbox-API-Arg"));
    //    try
    //    {
    //        data = System.Text.Encoding.ASCII.GetBytes(me);
    //        request.uploadHandler = (UploadHandler)new UploadHandlerRaw(data);

    //        Debug.Log("Uploaded?");
    //    }
    //    catch { }
    //    request.downloadHandler = (DownloadHandler)new DownloadHandlerBuffer();

    //    return request;
    //}
}

public class DropboxUploadParm {
    public string path;
    public bool autorename;
    public string mode;
}

public class DropBoxSharingSettings {
    //public string path;
    //public DropTest mPath = new DropTest();
    public string requested_visibility;
    public string audience;
    public string access;
}

[System.Serializable]
public class DropTest2
{
    public string path;
 
    //public bool short_url;
}

[System.Serializable]
public class DropTest {
    public string path;
    public DropBoxShareSettings settings = new DropBoxShareSettings();
    //public bool short_url;
}

[System.Serializable]
public class DropBoxShareSettings
{
    public string requested_visibility;
    public string audience;
    public string access;
}

public class DropBoxFolder {
    public string path,acl_update_policy;
    public bool force_async;
    public string member_policy, shared_link_policy, access_inheritance;
}

public class DropBoxGetFileData {
    public string file;
    public string[] actions;
}

public static class VMachineManger {
    public static string m_VMID;
}