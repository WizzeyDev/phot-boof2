﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AcstionLogic {
    public class VideoCaptureCTL : MonoBehaviour {
        #region Variables
        public float m_VideoCaptureTime;

         Evereal.VideoCapture.VideoCaptureCtrl m_VideoCapturCrl;
        bool isCaptureingVideo;
        #endregion
        void CaptureVideo() {
            m_VideoCapturCrl.StartCapture();
        }

        #region Init
        static VideoCaptureCTL instance;
        private void Awake() {
            instance = this;
        }

        // Start is called before the first frame update
        void Start() {
            if (m_VideoCapturCrl == null) {
                m_VideoCapturCrl = GetComponent<Evereal.VideoCapture.VideoCaptureCtrl>();
            }
    

            //CaptureVideo();
            //var videoCap = GetComponent<Evereal.VideoCapture.VideoCapture>().Cleanup();
        }


        #endregion


        void OnCaptureComplete() {
         
        }

        public TMPro.TextMeshProUGUI m_text;


        public GameObject test;
        // Update is called once per frame
        void Update() {
      
        
        }

        public void StartCaptureSequance() {
            m_VideoCapturCrl.StartCapture();

        }

        public void StopVideoTakeing() {
            m_VideoCapturCrl.StopCapture();
            //m_text.text = m_VideoCapturCrl.status.ToString();
        }

        public static bool ToggleCapture(bool isOn) {
            if (isOn) {
            instance.m_VideoCapturCrl.StartCapture();
                return true;

            }
            else {
                instance.m_VideoCapturCrl.StopCapture();
                return false;
            }
        }

        public static VideoCaptureCTL GetThis { get { return instance; } }
    }
}
