﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


public class WebCam : MonoBehaviour
{
    #region Singleton
    static WebCam single;
    private void Awake()
    {
        if (single == null)
            single = this;
    
    }

 
    #endregion

    #region Serializable Fields
    public RawImage displayImage;

    public Text DebugText;
    #endregion

    #region Private Fields
    private WebCamTexture webCameraTexture;

    private static string deviceName = string.Empty;
    #endregion



    #region Setup
    private void Start()
    {
        //CameraSelector.LoadDevicesNames(WebCamTexture.devices);
        //if (deviceName != string.Empty)
        //{
            SelectCamera(deviceName);
            //CameraSelector.SetCurrentDisplayName(deviceName);
        //}
        //else
        //    displayImage.color = Color.black;
    }
    #endregion

    private void Update() {
       // if (Input.GetKeyDown(KeyCode.D)) {
       //var size = new Vector2( Screen.width, Screen.height);
       // var rawImage = GetComponent<RectTransform>();
       // rawImage.sizeDelta = size;
       //     print(" Size 2 " + size);
       // }
    }
    

    private void SelectCamera(string cameraDeviceName)
    {
        var rawImage = GetComponent<RawImage>();
        Vector2 size = GetComponent<RectTransform>().sizeDelta = new Vector2(Screen.width, Screen.height);
            print(" Size " + size + " SizeDleta " + GetComponent<RectTransform>().sizeDelta);
        if (size ==new Vector2(1080,1920)) { //If portrait 
            print(" 3 " + size);
            //size = new Vector2( Screen.height, Screen.width);
            transform.rotation =  Quaternion.Euler(transform.rotation.x, 180, 90);
            GetComponent<RectTransform>().sizeDelta = new Vector2(Screen.height, Screen.width);

        }
     
        webCameraTexture = new WebCamTexture(1080, 1920);      
        //Vector2 size = GetComponent<RectTransform>().sizeDelta = new Vector2(1920, 1080);
        rawImage.material.mainTexture = webCameraTexture;
        webCameraTexture.Play();
        print(" W " + rawImage.material.mainTexture.width);
    }

    public void ToggleWebCam(bool isON) {
        if (isON) {
            webCameraTexture.Play();
        }
        else {
            webCameraTexture.Stop();
        }
    }

    public static void SelectNewCamera(string newDeviceName)
    {
        if(single.webCameraTexture != null)
            single.webCameraTexture.Stop();
        deviceName = newDeviceName;
        SceneManager.LoadScene(0);
    }

    public static void Restart() {
        if (single.webCameraTexture != null)
            single.webCameraTexture.Stop();
        SceneManager.LoadScene(0);
    }
    
}
