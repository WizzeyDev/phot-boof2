﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Mathematics;
using Unity.Jobs;
using Unity.Collections;
using Unity.Burst;

public class TaskForJobs : MonoBehaviour
{
    int numOfJobs = 10;



    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Alpha5)) {
            //EmailSender.SendEmailJob();
        }

        //float startTime = Time.realtimeSinceStartup;
        //NativeList<JobHandle> jobHandleList = new NativeList<JobHandle>(Allocator.Temp);
        ////RealyTouchTask();
        //for (int i = 0; i < numOfJobs; i++) {
        //    JobHandle jobHandle = ReallyToughTaskJob();
        //    jobHandleList.Add(jobHandle);
        //}
        //JobHandle.CompleteAll(jobHandleList);
        //jobHandleList.Dispose();
        //Debug.Log("ms" + ((Time.realtimeSinceStartup - startTime) * 1000));
    }

    void RealyTouchTask() {
        float value = 0f;
        for (int i = 0; i < 5000; i++) {
            value = math.exp10(math.sqrt(value));
        }
    }

    JobHandle ReallyToughTaskJob() {
        RealyToughJob job = new RealyToughJob();
        return job.Schedule();
    }
}

[BurstCompile]
public struct RealyToughJob :IJob {
    public void Execute() {
        float value = 0f;
        for (int i = 0; i < 5000; i++) {
            value = math.exp10(math.sqrt(value));
        }
    }
}