﻿using System;
using System.Collections.Generic;
using UnityEngine;
using com.bitmick.marshall.protocol;
using com.bitmick.marshall.models;

public class testy : MonoBehaviour {
         public static app.VMC_Test m_vend_machine;
    private void Start() {
        m_vend_machine = new app.VMC_Test();
        m_vend_machine.start();
    //var me = vmc_framework.getInstance(); //var m_test =;
        //print(me);
        //me.configure()
    }

    private void OnGUI() {
        Event e = Event.current;
        switch (e.character  ) {
            case '1':
                if (m_vend_machine.vmc_config.always_idle) {
                    // prepare single session object
                    m_vend_machine.session = new vmc_vend_t.vend_session_t(2000, 1, (byte)0, 10);
                    // do vend request
                    print("DA");
                    m_vend_machine.vmc_instance.vend.vend_request(m_vend_machine.session);
                }
                else {
                    m_vend_machine.session = null;
                    m_vend_machine.start_session(vmc_vend_t.session_type_credit_e);
                }
                break;
            case '2':
                m_vend_machine.stop_session(null);
                break;
            case '3':
                Debug.Log("Closeing");
                m_vend_machine.close_session(m_vend_machine.session);
                break;
            case '4': {
                vmc_vend_t.cash_sale_t data = new vmc_vend_t.cash_sale_t(0, 20);

                m_vend_machine.vmc_instance.vend.vend_cash_sale(data);
            }
            break;
            case '5':
                string ftp_url = "speedtest.tele2.net;anonymous;anonymous;/1KB.zip";
                m_vend_machine.vmc_instance.socket.open_socket(vmc_socket_t.vmc_socket_type_ftp_e, ftp_url, 21);
                break;
            case '6':
                m_vend_machine.vmc_instance.socket.open_socket(vmc_socket_t.vmc_socket_type_tcp_e, "1.2.3.4:5555", 80);
                break;
            case '7':
                byte[] tx_buf = new byte[100];
                m_vend_machine.vmc_instance.socket.transmit(tx_buf, 0, tx_buf.Length);
                break;
            case '8':
                m_vend_machine.vmc_instance.socket.close_socket();
                break;
            case '9':
                //m_vend_machine.vmc_instance.vend.
                break;
            case 's':
            case 'S':
                vmc_link.vpos_link_stats_t stats = vmc_framework.getInstance().link.get_stats();

                Debug.Log( String.Format(" ------ Stats ------ "));
                Debug.Log( String.Format("crc error: {0}", stats.crc_error));
                Debug.Log( String.Format("re-trans: {0}", stats.retrans));
                Debug.Log( String.Format("link loss: {0}", stats.comm_loss));
                Debug.Log(String.Format(" ------ Stats ------ "));
                break;
            default:
                break;
        }
        if (e.isKey) {
            Debug.Log("Detected key code: " + e.keyCode);
        }
    }
}

namespace app {
    public class VMC_Test : vmc_vend_t.vend_callbacks_t {
        public vmc_framework vmc_instance;
        public vmc_configuration vmc_config;
        public vmc_vend_t.vend_session_t session;


        public VMC_Test() {
            vmc_instance = vmc_framework.getInstance();

            vmc_config = new vmc_configuration();

            vmc_config.port_vpos = "COM3";
            vmc_config.port_vpos_baud = 115200;

            vmc_config.machine_type = vmc_configuration.machine_type_none;
            vmc_config.model = "marshall-cs-sdk";
            vmc_config.serial = "2434324019375404";
            vmc_config.sw_ver = "0.0.2.14";

            vmc_config.multi_session_support = false;
            vmc_config.price_not_final_support = false;
            vmc_config.reader_always_on = false;
            vmc_config.always_idle = false;

            vmc_config.mag_card_approved_by_vmc_support = false;
            vmc_config.mifare_approved_by_vmc_support = false;

            vmc_config.debug = true;
            vmc_config.dump_packets = false;

            vmc_instance.link.set_lowlevel(new lowlevel_serial());
            vmc_instance.configure(vmc_config);
            vmc_instance.vend.register_callbacks(this);
            vmc_instance.socket.register_callbacks(new socket_callbacks_t());
            vmc_instance.general.setOnTimeUpdateEvent(new time_update_callback());
        }

        public void start() {
            vmc_instance.start();
            //Log.d("--->", vmc_framework.get_version());

        }

        public void stop() {
            vmc_instance.stop();
        }

        public bool is_ready() {
            return vmc_instance.vend.is_ready();
        }

        public void start_session(int session_type) {
            vmc_instance.vend.session_start(session_type);
        }

        public void stop_session(vmc_vend_t.vend_session_t session) {
            if (!vmc_instance.vend.is_ready())
                vmc_instance.vend.session_cancel();
        }

        public void close_session(vmc_vend_t.vend_session_t session) {
            vmc_instance.vend.session_close(session);
        }

        public override void onReady(vmc_link.vpos_config_t config) {
            // vmc is ready for new session
            Debug.Log( "ready for new session");
            //TODO: Make system reopen it if its aint working

        }

        public override void onSessionBegin(int funds_avail) {
            Debug.Log( "session began. requesting product vend");

            if (vmc_config.multi_vend_support) {

                List<vmc_vend_t.vend_item_t> list = new List<vmc_vend_t.vend_item_t>();

                list.Add(new vmc_vend_t.vend_item_t((ushort)1, (ushort)10, (short)1, (byte)1));
                list.Add(new vmc_vend_t.vend_item_t((ushort)2, (ushort)20, (short)2, (byte)1));
                list.Add(new vmc_vend_t.vend_item_t((ushort)3, (ushort)30, (short)3, (byte)1));

                // prepare single session object
                session = new vmc_vend_t.vend_session_t(list);
            }
            else {
                // prepare single session object
                session = new vmc_vend_t.vend_session_t(2000, 1, (byte)0, 10);
            }

            // do vend request
            vmc_instance.vend.vend_request(session);


            // acknowledge mifare (note: only when machine is working on mifare/mag mode)
            if (vmc_config.mifare_approved_by_vmc_support || vmc_config.mag_card_approved_by_vmc_support)
                vmc_instance.vend.client_gateway_auth(true);
        }

        public override bool onVendApproved(vmc_vend_t.vend_session_t session) {
            Debug.Log( "vend approved");
            if (StagesController.GetScreen == StageScreen.ScreenSaver) {

                StagesController.NEXTScreen();
            }
            // true:  vend success
            // false: vend failure

            return true;
        }

        public override void onVendDenied(vmc_vend_t.vend_session_t session) {
            Debug.Log( "vend denied");
        }

        public override void onTransactionInfo(vmc_vend_t.vend_session_data_t data) {
            if (data.cc_last_4_digits != null) {
                Debug.Log( String.Format("received cc last 4 digits: {0}", data.cc_last_4_digits));
            }

            if (data.transaction_id != -1) {
                Debug.Log( String.Format("transaction id: {0}", data.transaction_id));
            }

            if (data.card_bin != null) {
                Debug.Log(String.Format("card bin: %s", data.card_bin));
            }

            if (data.prop_card_uid != null) {

                Debug.Log(String.Format("prop card uid: {0}", data.prop_card_uid));
            }

        }

        public override void onSettlement(bool success) {
            Debug.Log(string.Format("settlement: {0}", success ? "success" : "fail"));
        }
    }



    public class time_update_callback : vmc_general_t.TimeUpdateEvents {
        public override void onTimeUpdate(byte year, byte month, byte day, byte hours, byte minutes, byte seconds) {
            Debug.Log( String.Format("receveid time: {0}/{1}/{2} {3}:{4}:{5}", day, month, year, hours, minutes, seconds));
        }
    }

    public class socket_callbacks_t : vmc_socket_t.socket_callbacks_t {

        int received_bytes = 0;

        public override void onReady() {
        }

        public override bool onReceive(byte[] buffer, int size) {
            received_bytes += size;

            Debug.Log(String.Format("received: {0} bytes, total: {1}", size, received_bytes));

            // you can save the bytes into a file (for FTP transfer for example)

            return true;
        }

        public override void onFTPDone(bool success) {
            Debug.Log( string.Format("finished FTP reception, status: {0}", success ? "success" : "fail"));

            received_bytes = 0;
        }

        public override void onSocketOpened() {
            Debug.Log("socket is opened. on tcp socket, listening...");
        }

        public override void onSocketClosed() {
            Debug.Log("socket closed...");
        }

        public override void onSocketError() {

        }

        public override void onTimerTick(long ms) {

        }

        public override void onTransmitDone(bool success) {
            Debug.Log("transmitted data");

        }
    }
}
